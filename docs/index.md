---
lang: zh-CN
title: 首页
description: 工作、学习、生活分享
home: true
footer: MIT Licensed | Copyright © 2022-present nigel
---

# 工作、学习、生活分享

热爱篮球，喜欢动画，自学过设计，爱好广泛，在技术上也喜欢广泛探索，目前做过小程序、H5、PC端、服务端、谷歌插件、VS Code插件开发经验。对前端知识学习和理解不断增强。随着自己接触的前端领域知识越来越多、应用越来越广泛。想着梳理下自己知识体系，记录平时学习所得，方便复盘和整理总结。