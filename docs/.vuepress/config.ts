import { defineUserConfig } from 'vuepress'
import type { DefaultThemeOptions } from 'vuepress'
import { path } from '@vuepress/utils'
import { navbar, sidebar } from './configs'
const isProd = process.env.NODE_ENV === 'production'
const REPO = '/nigel-tech-blog/'
export default defineUserConfig<DefaultThemeOptions>({
  base: isProd ? REPO : '/',
  // 站点配置
  lang: 'zh-CN',
  title: '技术博客',
  description: '记录工作、学习所思所得',
  // 主题和它的配置
  theme: '@vuepress/theme-default',
  // cache: false,
  // open: true,
  markdown: {
    importCode: {
      handleImportPath: (str) =>
        str.replace(
          /^@vuepress/,
          path.resolve(__dirname, '../../node_modules/@vuepress')
        ),
    },
  },
  themeConfig: {
    logo: '/images/logo.png',
    docsDir: 'docs',
    locales: {
      '/': {
        // navbar
        navbar: navbar.zh,
        // sidebar
        sidebar: sidebar.zh,
        // page meta
        editLinkText: 'Edit this page on GitHub',
      }
    },
  },
  head: [
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: `/images/icons/favicon-16x16.png`,
      },
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: `/images/icons/favicon-32x32.png`,
      },
    ],
    ['link', { rel: 'manifest', href: '/manifest.webmanifest' }],
    ['meta', { name: 'application-name', content: 'nigel技术博客' }],
    ['meta', { name: 'apple-mobile-web-app-title', content: 'nigel技术博客' }],
    [
      'meta',
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
    ],
    [
      'link',
      { rel: 'apple-touch-icon', href: `/images/icons/apple-touch-icon.png` },
    ],
    [
      'link',
      {
        rel: 'mask-icon',
        href: '/images/icons/safari-pinned-tab.svg',
        color: '#3eaf7c',
      },
    ],
    ['meta', { name: 'msapplication-TileColor', content: '#3eaf7c' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
  ],
  
})